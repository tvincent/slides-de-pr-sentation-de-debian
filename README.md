# Slides de présentation de Debian

Ce projet regroupe tous les supports des conférences animées par un membre de l'association et visant à présenter Debian ou à en parler.

## Installer les dépendances

```bash
# Pour la version HTML
apt install pandoc

# Pour la version PDF, en plus il faut :
apt install texlive-latex-base texlive-latex-recommended texlive-latex-extra
```

## Compiler la présentation

Sur Stretch (Debian 9) ou Buster (Debian 10) :

```bash
# Pour la version HTML
pandoc -s --slide-level=2 -t revealjs cdl2019.md -o cdl2019.html

# Pour la version PDF
pandoc -t beamer cdl2019.md -o cdl2019.pdf
```

## Exporter en PDF depuis la version HTML

Une fois le fichier compilé au format HTML :
- L'ouvrir avec **Chromium**,
- Ajouter `?print-pdf` à la fin de l'adresse URL **/talk.html** - [lien à venir](),
- **CTRL** + **p** (pour imprimer),
- Destination : sauvegarder en PDF,
- Options : cocher la case **arrière plan graphiques**,
- Terminer en cliquant sur **Sauvegarder**.

Note : L'export au format PDF ne fonctionne pas correctement avec **Firefox**

